DATE=`date +%Y-%m-%d---%H-%M-%S`

ctan:
	mkdir -p dist/biblatex2bibitem
	rm -rf dist/biblatex2bibitem/*
	cp -t dist/biblatex2bibitem *.tex *.sty *.bib *.pdf *.md *.txt
	cd dist; zip -r biblatex2bibitem-$(DATE).zip biblatex2bibitem

clean:
	trash-put -f *.aux *.bbl *.bcf *.blg *.fdb_latexmk *.fls *.log *.out *.run.xml
